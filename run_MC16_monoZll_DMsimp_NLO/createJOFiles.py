#!/usr/bin/env python
import os
import sys
import argparse as ap

from wCalc import swCalc
sys.path.append(os.path.abspath(os.path.curdir))

# python module to create jobOption files and command file


###############################################################################
cmd_template = """
./prun_monoZll.sh %(RunNum)s %(JO)s %(dataset)s %(jobs)s %(version)s
"""
###############################################################################

###############################################################################

recovery_template="""
#rucio download %(dataset)s_EXT0/ --dir EVT/                                                                                                                 
rucio download %(dataset)s.log/ --dir Logs/                                                                                                                  
rucio download %(dataset)s_DAOD_TRUTH1.daod.root --dir DAOD                                                                                                  
rucio download %(dataset)s_tmp_run_01._00001.events.events/ --dir LHE  
"""
###############################################################################

# mass points
masses = []
# masses.append([mMed,mDM, lhe_event_multiplier])

masses.append([200,50,1.5])
"""
masses.append([300,50,1.5])
masses.append([500,50,1.5])
masses.append([600,50,1.5])
masses.append([900,50,1.5])
masses.append([500,100,1.5])
masses.append([600,100,1.5])
masses.append([900,100,1.5])
masses.append([500,100,1.5])
masses.append([1050,100,1.5])
masses.append([300,100,1.5])
masses.append([1050,50,1.5])
masses.append([1200,50,1.5])
masses.append([1200,100,1.5])
masses.append([500,150,1.5])
masses.append([500,200,1.5])
masses.append([600,150,1.5])
masses.append([600,200,1.5])
masses.append([600,250,1.5])
masses.append([750,150,1.5])
masses.append([750,200,1.5])
masses.append([900,150,1.5])
masses.append([900,200,1.5])
masses.append([900,250,1.5])
masses.append([900,300,1.5])
masses.append([900,350,1.5])
masses.append([1050,150,1.5])
masses.append([1050,200,1.5])
masses.append([1050,250,1.5])
masses.append([1050,300,1.5])
masses.append([1050,350,1.5])
masses.append([1200,150,1.5])
masses.append([1200,200,1.5])
masses.append([1200,250,1.5])
masses.append([1200,300,1.5])
masses.append([1200,350,1.5])
masses.append([650,75,1.5])
masses.append([650,125,1.5])
masses.append([650,175,1.5])
masses.append([650,225,1.5])
"""

###############################################################################

def createNloJOFiles(MGControlFile, jobs, version):
    model = var.model
    typeMed = var.typeMed
    gQ = var.gQ
    gL = var.gL
    gDM = var.gDM

    jo_template = var.jo_template


    # create command files and recovery files
    cmd_filename = "batch_panda_" + model + ".sh"
    fc = open(cmd_filename, 'w')
    
    fc.write("#asetup 19.2.5.29.1,MCProd,here\n")
    fc.write("#lsetup rucio \n")
    fc.write("#lsetup panda \n")

    rec_filename = "recovery_" + model + ".sh"
    fr = open(rec_filename, 'w')

    #dictionary to replace strings

    kw = {}

    run_number = 310637

    for mass in masses:
        # calculate width
        width =  swCalc(typeMed, gQ, gL, gDM, mass[0], mass[1])

        # replace strings

        kw['model'] = model
        kw['MGControl'] = MGControlFile
        kw['jobs'] = str(jobs)
        kw['version'] = version
        kw['gQ'] = gQ
        kw['gL'] = gL
        kw['gDM'] = gDM
        kw['mDM'] = mass[1]
        kw['mMed'] = mass[0]
        kw['lhe_event_multiplier'] = mass[2]
        kw['wMed'] = width
        kw['RunNum'] = run_number
    
        jo_str = jo_template  % kw

        #create JobOption file
        jo_filename = "MC15.%(RunNum)s.aMcAtNloPy8EG_N30NLO_A14N23LO_DMsimp%(model)s_Zll_MET40_DM%(mDM)s_MM%(mMed)s.py" %kw
        dataset_name = "user.cranelli.JIRA_NLO_DMsimp%(model)s_Zll_MET40_DM%(mDM)s_MM%(mMed)s_NoPSWeights.%(version)s" %kw

        kw["JO"] = jo_filename
        kw['dataset'] = dataset_name

        f = open(jo_filename, 'w')
        f.write(jo_str)
        f.close()

        #create command and write it
        command = cmd_template % kw
        fc.write(command)

        #create recovery and write it
        recovery = recovery_template % kw
        fr.write(recovery)
        
        run_number += 1

    fc.close()
    fr.close()


if __name__ == '__main__':

    parser = ap.ArgumentParser(description = 'model : Benchmark DMSimp model with '
                               'fixed values for gQ, gL, and gDM. \n'
                               'version : version number vXX, to distinguish the datsets'
                               'submitted to the grid',
                               formatter_class=ap.RawTextHelpFormatter)

    parser.add_argument( "model" , type=str, help="Benchmark DMSimp model")
    parser.add_argument( "version" , type=str, help="dataset version")
    parser.add_argument( '--jobs' , type=int, help="number of jobs to run")
    parser.add_argument( '--MGControl', type=str, help="MG Control File to use")

    args = parser.parse_args()

    if len(sys.argv) < 3:
        print "\n ERROR model name and dataset version required \n"
        quit()
        
    if(args.jobs==None): args.jobs=1
    if(args.MGControl==None): args.MGControl = "MC15JobOptions/MadGraphControl_aMcAtNloPythia8EG_NN30NLO_A14N23LO_DMsimp_monoZll_MET40.py"

    model = args.model
    if not (args.model in ["A1", "A2", "V1", "V2"] ) :
        print '\n model name must be A1, A2, V1, or V2\n\n'
        quit()
        
    if args.model == "A1": 
        import model_A1 as var

    if args.model == "A2":
        import model_A2 as var

    if args.model == "V1":
        import model_V1 as var

    if args.model == "V2":
        import model_V2 as var
        

    createNloJOFiles(args.MGControl, args.jobs, args.version)
    
