#!/bin/bash
# Sets up ATLAS environment

RunNumber=$1
JO=$2
RDMSEED=$3

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
source $AtlasSetup/scripts/asetup.sh  19.2.5.29.1,MCProd,here
export MAKEFLAGS="-j1 QUICK=1 -l8"
export ATHENA_PROC_NUMBER=8

## Event Generation      

Generate_tf.py --ecmEnergy 13000 \
 --runNumber ${RunNumber} --firstEvent 1 \
 --maxEvents 10000 --randomSeed $RDMSEED \
 --jobConfig $JO \
 --outputEVNTFile evt.pool.root

# Convert to Truth DAOD
# run with 1 core
unset ATHENA_PROC_NUMBER
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
source $AtlasSetup/scripts/asetup.sh 20.7.8.2,AtlasDerivation,here

Reco_tf.py --inputEVNTFile evt.pool.root --outputDAODFile daod.root --reductionConf TRUTH1