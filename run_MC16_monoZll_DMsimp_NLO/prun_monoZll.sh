#!/bin/bash

#To Run:
# lsetup rucio 
# voms-proxy-init -voms atlas 
# lsetup panda
# ./prun_monoZll_V1.sh 100 600

RunNumber=$1
JO=$2
dataset_name=$3
JOBS=$4
version=$5


if [ -f $JO ]
then 
    athenaTag="19.2.5.29.1"
    output="tmp_run_01._00001.events.events,evt.pool.root,DAOD_TRUTH1.daod.root"

    echo  ${JO} ${output} ${dataset_name} >> submission.${version}.log

    prun \
	--outDS=${dataset_name} \
	--allowTaskDuplication \
	--athenaTag=$athenaTag --nJobs ${JOBS}  \
	--unlimitNumOutputs --long --exec="sh run_evgen_truth_monoZll.sh ${RunNumber} ${JO} %RNDM:100" \
	--extFile $JO \
	--outputs=${output} >> submission.${version}.log 2>&1

# --disableAutoRetry \
    #--allowTaskDuplication \
    #--cmtConfig=$cmtConfig \
    #--skipScout 

else
    echo "No JO File Found"
fi