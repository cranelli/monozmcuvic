model="V2"

#Fixed Values
typeMed = "vector"
gQ = 0.1
gL = 0.01
gDM = 1.0

###############################################################################
jo_template = """MXd =  %(mDM)s.
MY1 =  %(mMed)s.
gVXd =  %(gDM)s #vector coupling to DM 
gAXd = 0. #axial coupling to DM 
gAd11 = 0. #axial couplings to quarks
gAu11 = 0.
gAd22 = 0.
gAu22 = 0.
gAd33 = 0.
gAu33 = 0.
gAl11 = 0.
gAl22 = 0.
gAl33 = 0.

gVd11 = %(gQ)s #vector couplings to quarks 
gVu11 = %(gQ)s
gVd22 = %(gQ)s
gVu22 = %(gQ)s
gVd33 = %(gQ)s
gVu33 = %(gQ)s
gVl11 = %(gL)s
gVl22 = %(gL)s
gVl33 = %(gL)s
gnu11 = %(gL)s
gnu22 = %(gL)s
gnu33= %(gL)s
WY1 = %(wMed)s

PartonShowerJO = "%(parton_shower_jo)s" #Used For PS Sytematics 
LHE_EventMultiplier = %(lhe_event_multiplier)s #For Filter, Multiplier on maxEvents    
include("%(MGControl)s")  
"""
###############################################################################

