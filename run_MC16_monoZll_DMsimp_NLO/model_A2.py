model="A2"

#Fixed Values
typeMed = "axial"
gQ = 0.1
gL = 0.1
gDM = 1.0

###############################################################################
jo_template = """MXd =  %(mDM)s.
MY1 =  %(mMed)s.
gVXd = 0. #vector coupling to DM 
gAXd = %(gDM)s #axial coupling to DM 
gAd11 = %(gQ)s #axial couplings to quarks
gAu11 = %(gQ)s
gAd22 = %(gQ)s
gAu22 = %(gQ)s
gAd33 = %(gQ)s
gAu33 = %(gQ)s
gAl11 = %(gL)s
gAl22 = %(gL)s
gAl33 = %(gL)s

gVd11 = 0. #vector couplings to quarks 
gVu11 = 0.
gVd22 = 0.
gVu22 = 0.
gVd33 = 0.
gVu33 = 0.
gVl11 = 0.
gVl22 = 0.
gVl33 = 0.
gnu11 = -%(gL)s
gnu22 = -%(gL)s
gnu33= -%(gL)s
WY1 = -%(wMed)s

PartonShowerJO = "%(parton_shower_jo)s" #Used For PS Sytematics 
LHE_EventMultiplier = %(lhe_event_multiplier)s #For Filter, Multiplier on maxEvents    
include("%(MGControl)s")  
"""
###############################################################################
