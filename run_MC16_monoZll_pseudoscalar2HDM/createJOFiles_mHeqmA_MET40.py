#!/usr/bin/env python
import os
import sys
import benchmark_mHeqmA_MET40 as benchmark
import argparse as ap

sys.path.append(os.path.abspath(os.path.curdir))

# python script to create jobOption files and command files

###############################################################################
cmd_template = """
#./submit_2HDMa_Generate.sh %(RunNum)s %(JO)s %(dataset)s %(jobs)s %(ver)s
sleep 1s
./submit_2HDMa.sh  %(RunNum)s %(JO)s %(dataset)s %(jobs)s %(ver)s
#bsub -q 2nw4cores run_job.sh %(RunNum)s %(JO)s %(dataset)s %(jobs)s
"""                                                             
###############################################################################

rec_template="""
#rucio download %(dataset)s_evt.pool.root/ --dir EVT/
rucio download %(dataset)s.log/ --dir Logs/
rucio download %(dataset)s_DAOD_TRUTH3.pool.root --dir DAOD 
rucio download %(dataset)s_tmp_run_01._00001.events.events/ --dir LHE
"""

###############################################################################

# mass points
masses = []

# Assum mH = mA = mH+
#masses.append([tanb, sinp mH, ma, mDM, lhe_event_multiplier])

#mA - ma Grid

lhe_event_multiplier = 1.1
tanb = 1.0
sinp = 0.35
mDM  = 10

masses.append([tanb, sinp, 350, 200, mDM, 1.2])
masses.append([tanb, sinp, 1000, 350, mDM, lhe_event_multiplier])
masses.append([tanb, sinp, 225, 100, mDM, 2])
masses.append([tanb, sinp, 700, 400, mDM, lhe_event_multiplier])
masses.append([tanb, sinp, 800, 450, mDM, lhe_event_multiplier])

#tanb - ma  grids (sinp = 0.35 mA = 600 GeV, 0.707 mA = 1TeV)
#masses.append([tanb, sinp mH, ma, lhe_event_multiplier])
"""
masses.append([0.3, 0.35, 600, 200, 10, lhe_event_multiplier]) 
masses.append([0.5, 0.35, 600, 200, 10, lhe_event_multiplier]) 
masses.append([1.0, 0.35, 600, 200, 10, lhe_event_multiplier]) 
masses.append([5.0, 0.35, 600, 200, 10, lhe_event_multiplier]) 
masses.append([10.0, 0.35, 600, 200, 10, lhe_event_multiplier]) 
masses.append([20.0, 0.35, 600, 200, 10, 1.2]) 
"""

"""
lhe_event_multiplier = 1.1
mDM=10
for sinp, mH in [ (0.707, 1000) ]:
    for ma in range(150, 501, 100):
        masses.append([0.5, sinp, mH, ma, mDM, lhe_event_multiplier])
        masses.append([1.0, sinp, mH, ma, mDM, lhe_event_multiplier])
        #masses.append([1.5, sinp, mH, ma, mDM, lhe_event_multiplier])
        masses.append([2.0, sinp, mH, ma, mDM, lhe_event_multiplier])
        #masses.append([2.5, sinp, mH, ma, mDM, lhe_event_multiplier])
        masses.append([3.0, sinp, mH, ma, mDM, lhe_event_multiplier])
        #masses.append([3.5, sinp, mH, ma, mDM, lhe_event_multiplier])
        #masses.append([4.0, sinp, mH, ma, mDM, lhe_event_multiplier])
        #masses.append([4.5, sinp, mH, ma, mDM, lhe_event_multiplier])
        masses.append([5.0, sinp, mH, ma, mDM, lhe_event_multiplier])
        #masses.append([6.0, sinp, mH, ma, mDM, lhe_event_multiplier])
        #masses.append([8.0, sinp, mH, ma, mDM, lhe_event_multiplier])
        masses.append([10.0, sinp, mH, ma, mDM, lhe_event_multiplier])
        #masses.append([15.0, sinp, mH, ma, mDM, lhe_event_multiplier])
        masses.append([20.0, sinp, mH, ma, mDM, lhe_event_multiplier])
"""






#sinp scans
#masses.append([tanb, sinp mH, ma, mDM, lhe_event_multiplier])
"""
#sinps = [ 0.1, 02, 0.3, 0.35, 0.4, 0.5, 0.6, 0.7, 0.707, 0.8, 0.9 ]
sinps = [ 0.1, 0.35, 0.5, 0.7, 0.9 ]
for sinp in sinps:
    #masses.append([1.0, sinp, 600, 200, 10, lhe_event_multiplier])
    masses.append([1.0, sinp, 1000, 350, 10, lhe_event_multiplier])
"""

"""
#tanb scans
#tanbs = [ 0.3, 0.5, 1, 2, 3, 5, 10, 20 ]
tanbs = [ 0.3, 1, 2, 5, 10, 20 ]
sinps = [ 0.35, 0.7 ]
mDM=10
for tanb in tanbs:
    for sinp in sinps:
        #masses.append([tanb, sinp, 300,  250, mDM, lhe_event_multiplier]) 
        masses.append([tanb, sinp, 400,  250, mDM, lhe_event_multiplier]) 
        #masses.append([tanb, sinp, 500,  250, mDM, lhe_event_multiplier]) 
        #masses.append([tanb, sinp, 600,  250, mDM, lhe_event_multiplier]) 
        #masses.append([tanb, sinp, 700,  250, mDM, lhe_event_multiplier]) 
        #masses.append([tanb, sinp, 800,  250, mDM, lhe_event_multiplier]) 
        #masses.append([tanb, sinp, 900,  250, mDM, lhe_event_multiplier]) 
        masses.append([tanb, sinp, 1000, 250, mDM, lhe_event_multiplier]) 
        #masses.append([tanb, sinp, 1250, 250, mDM, lhe_event_multiplier]) 
        masses.append([tanb, sinp, 1500, 250, mDM, lhe_event_multiplier]) 
"""

"""
lhe_event_multiplier = 1.1
tanb=1.0
mH = 600
ma = 250
masses.append([tanb, 0.1, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.2, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.3, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.4, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.5, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.6, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.7, mH, ma, lhe_event_multiplier])
#masses.append([tanb, 0.707, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.8, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.9, mH, ma, lhe_event_multiplier])

mH=700
ma = 350
masses.append([tanb, 0.1, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.2, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.3, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.4, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.5, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.6, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.7, mH, ma, lhe_event_multiplier])
#masses.append([tanb, 0.707, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.8, mH, ma, lhe_event_multiplier])
masses.append([tanb, 0.9, mH, ma, lhe_event_multiplier])
"""

#mDM Scan (ma > 350 GeV)
"""
lhe_event_multiplier = 1.1 
tanb=1.0
sinp=0.707
mH = 750
ma = 400  

masses.append([tanb, sinp, mH, ma, 10, lhe_event_multiplier]) 
#masses.append([tanb, sinp, mH, ma, 100, lhe_event_multiplier]) 
masses.append([tanb, sinp, mH, ma, 150, lhe_event_multiplier]) 
#masses.append([tanb, sinp, mH, ma, 165, lhe_event_multiplier]) 
masses.append([tanb, sinp, mH, ma, 175, lhe_event_multiplier]) 
#masses.append([tanb, sinp, mH, ma, 185, lhe_event_multiplier]) 
masses.append([tanb, sinp, mH, ma, 195, lhe_event_multiplier]) 
"""

def create2HDMaJOFiles(MGControlFile, jobs, version):
    
    jo_template = benchmark.jo_template
    
    cmd_filename = "batch.sh"
    fc = open(cmd_filename, 'w')
    #fc.write("#asetup 19.2.5.33.3,MCProd,here\n")
    fc.write("#asetup 20.7.9.9.27,MCProd,here\n")
    fc.write("#lsetup rucio \n")
    fc.write("#lsetup panda \n")


    rec_filename = "recovery."+ version +".sh"
    fr = open(rec_filename, 'w')

    # dictionary to replace strings
    kw = {}
    kw['MGControl'] = MGControlFile
    kw['ver'] = version
    kw["jobs"] = jobs
    kw["parton_shower_jo"] = "" # Is specified in MG Control File, only used for PS systematics.

    #Run Nummber
    run_number = 999999

    for mass in masses:    

        # replace strings
        kw['tanb'] = mass[0]
        kw['sinp'] = mass[1]
        kw['mH'] = mass[2]
        #kw['mA'] = mass[3]
        #kw['mHc'] = mass[4]
        kw['ma'] = mass[3]
        kw['mDM'] = mass[4]
        kw['lhe_event_multiplier'] = mass[5]

        kw['tanb_s'] = str(mass[0]).replace(".", "p")
        kw['sinp_s'] = str(mass[1]).replace(".", "p")
        # kw["lam3_s"] = str(mass[6]).replace(".", "d")


        for initial_state in [ "gg", "bb"]:
            # Generate bb initiated samples for tanb > 1.0
            #if initial_state == "bb" and  float(kw['tanb']) <= 1.0: continue
                
            kw['RunNum'] = run_number
            kw["IS"] = initial_state
            jo_str = jo_template  % kw
    
            # create JobOption file
            jo_filename =  "MC15.%(RunNum)s.MGPy8EG_2HDMa_%(IS)s_MonoZ_ll_tb%(tanb_s)s_sp%(sinp_s)s_mH%(mH)s_ma%(ma)s_mDM%(mDM)s.py" % kw
            dataset_name = "user.cranelli.2HDMa_%(IS)s_MonoZ_ll_MET40_tb%(tanb_s)s_sp%(sinp_s)s_mH%(mH)s_ma%(ma)s_mDM%(mDM)s.%(ver)s" % kw

            kw['JO'] = jo_filename
            kw['dataset'] = dataset_name
   
            f = open(jo_filename, 'w')
            f.write(jo_str)
            f.close()
    
            command = cmd_template % kw
            fc.write(command)

            recovery = rec_template %kw
            fr.write(recovery)
    
            #run_number += 1

    fc.close()
    fr.close()


if __name__ == '__main__':
    
    parser = ap.ArgumentParser(description = 'Create JO Files to be used for event generation of the pseudoscalar2HDM model',
                               formatter_class=ap.RawTextHelpFormatter)

    parser.add_argument( 'version' , type=str, help="version number vXX to distinguish datasets submitted to the grid")
    parser.add_argument( '--jobs' , type=int, help="number of jobs (of 10,000 events each) submitted")
    parser.add_argument( '--MGControl', type=str, help="MG Control File to use")

    args = parser.parse_args()

    if len(sys.argv) < 2:
        print "\n ERROR version required to distinguish datasets \n"

    if(args.jobs==None): args.jobs=1
    if(args.MGControl==None): args.MGControl = "MC15JobOptions/MadGraphControl_Pythia8EvtGen_NNPDF30nlo_A14NNPDF23LO_2HDMa_ll_MET40.py"



    create2HDMaJOFiles(args.MGControl, args.jobs, args.version)


#masses.append([1.0, 0.35, 100, 100, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 200, 100, 10, 3])
#masses.append([1.0, 0.35, 225, 100, 10, 2])
#masses.append([1.0, 0.35, 350, 100, 10, 1.5])
#masses.append([1.0, 0.35, 400, 100, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 450, 100, 10, 1.1])
#masses.append([1.0, 0.35, 500, 100, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 550, 100, 10, 1.1])
#masses.append([1.0, 0.35, 600, 100, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 700, 100, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35,  800, 100, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 900, 100, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1000, 100, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1100, 100, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1200, 100, 10, lhe_event_multiplier])

#masses.append([1.0, 0.35, 200, 150, 10, 3])
#masses.append([1.0, 0.35, 400, 150, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 450, 150, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 500, 150, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 550, 150, 10, lhe_event_multiplier])

#masses.append([1.0, 0.35, 100, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 200, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 250, 200, 10, 3])
#masses.append([1.0, 0.35, 300, 200, 10, lhe_event_multiplier])
##masses.append([1.0, 0.35, 350, 200, 10, 1.25])
#masses.append([1.0, 0.35, 400, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 450, 200, 10, 1.25])
#masses.append([1.0, 0.35, 500, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 550, 200, 10, 1.25])
#masses.append([1.0, 0.35, 600, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 700, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35,  800, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35,  900, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1000, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1100, 200, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1200, 200, 10, lhe_event_multiplier])

#masses.append([1.0, 0.35, 100, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 200, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 250, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 300, 250, 10, 3])
#masses.append([1.0, 0.35, 400, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 450, 250, 10, 1.25])
#masses.append([1.0, 0.35, 500, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 550, 250, 10, 1.25])
#masses.append([1.0, 0.35, 600, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 700, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35,  800, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35,  900, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1000, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1100, 250, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1200, 250, 10, lhe_event_multiplier])

#masses.append([1.0, 0.35, 200, 300, 10, 1.25])
#masses.append([1.0, 0.35, 250, 300, 10, 1.25])
#masses.append([1.0, 0.35, 300, 300, 10, 3])
#masses.append([1.0, 0.35, 350, 300, 10, 4])
#masses.append([1.0, 0.35, 400, 300, 10, 3])
#masses.append([1.0, 0.35, 450, 300, 10, 3])
#masses.append([1.0, 0.35, 1000, 300, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1100, 300, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35,  1200, 300, 10, lhe_event_multiplier])

#masses.append([1.0, 0.35, 400, 350, 10, 4])
#masses.append([1.0, 0.35, 450, 350, 10, 3])
##masses.append([1.0, 0.35, 1000, 350, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1100, 350, 10, lhe_event_multiplier]

#masses.append([1.0, 0.35, 100, 400, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 200, 400, 10, 1.25])
#masses.append([1.0, 0.35, 250, 400, 10, 3])
#masses.append([1.0, 0.35, 300, 400, 10, 1.25])
#masses.append([1.0, 0.35, 350, 400, 10, 2])
#masses.append([1.0, 0.35, 400, 400, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 450, 400, 10, 3])
#masses.append([1.0, 0.35, 500, 400, 10, 3])
#masses.append([1.0, 0.35, 550, 400, 10, 1.5])
#masses.append([1.0, 0.35, 700, 400, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 900, 400, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1000, 400, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1100, 400, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1200, 400, 10, lhe_event_multiplier])

#masses.append([1.0, 0.35, 600, 450, 10, 1.5])
#masses.append([1.0, 0.35, 700, 450, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 800, 450, 10, lhe_event_multiplier])

#masses.append([1.0, 0.35, 100, 500, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 200, 500, 10, lhe_event_multiplier])
##masses.append([1.0, 0.35, 300, 500, 10, 1.25])
##masses.append([1.0, 0.35, 400, 500, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 500, 500, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 600, 500, 10, 3])
#masses.append([1.0, 0.35, 700, 500, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 800, 500, 10, lhe_event_multiplier])
##masses.append([1.0, 0.35, 700, 500, 10, lhe_event_multiplier])
##masses.append([1.0, 0.35, 1000, 500, 10, lhe_event_multiplier])
##masses.append([1.0, 0.35, 1100, 500, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1200, 500, 10, lhe_event_multiplier])

#masses.append([1.0, 0.35, 100, 600, 10, lhe_event_multiplier])
##masses.append([1.0, 0.35, 200, 600, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 300, 600, 10, lhe_event_multiplier])
##masses.append([1.0, 0.35, 400, 600, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 500, 600, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 600, 600, 10, lhe_event_multiplier])
##masses.append([1.0, 0.35, 700, 600, 10, 4])
#masses.append([1.0, 0.35, 800, 600, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 700, 600, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1000, 600, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1100, 600, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 1200, 600, 10, lhe_event_multiplier])


#Emulation Points
#masses.append([1.0, 0.35, 1090, 100, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 990, 100, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 885, 325, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 815, 325, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 780, 325, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 720, 325, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 680, 325, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 620, 325, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 680, 275, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 620, 275, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 580, 275, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 520, 275, 10, lhe_event_multiplier]) 

#masses.append([1.0, 0.35, 700, 265, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 600, 280, 10, lhe_event_multiplier]) 
#masses.append([1.0, 0.35, 800, 325, 10, lhe_event_multiplier])
#masses.append([1.0, 0.35, 900, 340, 10, lhe_event_multiplier])

#masses.append([0.5, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([1.0, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([1.5, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([2.0, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([2.5, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([3.0, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([3.5, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([4.0, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([4.5, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([5.0, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([6.0, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([8.0, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([10.0, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([15.0, sinp, mH, 100, mDM, lhe_event_multiplier])
#masses.append([20.0, sinp, mH, 100, mDM, lhe_event_multiplier])

#masses.append([0.5, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([1.0, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([1.5, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([2.0, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([2.5, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([3.0, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([3.5, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([4.0, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([4.5, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([5.0, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([6.0, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([8.0, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([10.0, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([15.0, sinp, mH, 150, mDM, lhe_event_multiplier])
#masses.append([20.0, sinp, mH, 150, mDM, lhe_event_multiplier])
 
#masses.append([0.5, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([1.0, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([1.5, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([2.0, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([2.5, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([3.0, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([3.5, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([4.0, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([4.5, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([5.0, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([6.0, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([8.0, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([10.0, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([15.0, sinp, mH, 200, mDM , lhe_event_multiplier])
#masses.append([20.0, sinp, mH, 200, mDM , lhe_event_multiplier])

#masses.append([0.5, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([1.0, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([1.5, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([2.0, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([2.5, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([3.0, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([3.5, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([4.0, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([4.5, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([5.0, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([6.0, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([8.0, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([10.0, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([15.0, sinp, mH, 250, mDM , lhe_event_multiplier])
#masses.append([20.0, sinp, mH, 250, mDM , lhe_event_multiplier])

#masses.append([0.5, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([1.0, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([1.5, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([2.0, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([2.5, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([3.0, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([3.5, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([4.0, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([4.5, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([5.0, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([6.0, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([8.0, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([10.0, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([15.0, sinp, mH, 300, mDM , lhe_event_multiplier])
#masses.append([20.0, sinp, mH, 300, mDM , lhe_event_multiplier])

#masses.append([0.5, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([1.0, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([1.5, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([2.0, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([2.5, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([3.0, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([3.5, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([4.0, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([4.5, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([5.0, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([6.0, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([8.0, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([10.0, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([15.0, sinp, mH, 350, mDM , lhe_event_multiplier])
#masses.append([20.0, sinp, mH, 350, mDM , lhe_event_multiplier])

#masses.append([0.5, sinp, mH, 400, mDM, 2])
#masses.append([1.0, sinp, mH, 400, mDM, lhe_event_multiplier])
#masses.append([1.5, sinp, mH, 400, mDM, 2])
#masses.append([2.0, sinp, mH, 400, mDM, 2])
#masses.append([2.5, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([3.0, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([3.5, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([4.0, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([4.5, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([5.0, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([6.0, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([8.0, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([10.0, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([15.0, sinp, mH, 400, mDM , lhe_event_multiplier])
#masses.append([20.0, sinp, mH, 400, mDM , lhe_event_multiplier])

#masses.append([0.5, sinp, mH, 450, mDM, 1.5])
#masses.append([1.5, sinp, mH, 450, mDM, 1.5])
#masses.append([2.0, sinp, mH, 450, mDM, 1.5])
#masses.append([2.5, sinp, mH, 450, mDM, 1.5])
#masses.append([3.0, sinp, mH, 450, mDM, 1.5])
#masses.append([3.5, sinp, mH, 450, mDM, 1.5])
#masses.append([4.0, sinp, mH, 450, mDM, 1.5])
#masses.append([4.5, sinp, mH, 450, mDM, 1.5])
#masses.append([5.0, sinp, mH, 450, mDM, 1.5])

#masses.append([0.5, sinp, mH, 500, mDM, 3])
#masses.append([1.5, sinp, mH, 500, mDM, 3])
#masses.append([2.0, sinp, mH, 500, mDM, 3])
#masses.append([2.5, sinp, mH, 500, mDM, 3])
#masses.append([3.0, sinp, mH, 500, mDM, 3])
#masses.append([3.5, sinp, mH, 500, mDM, 3])
#masses.append([4.0, sinp, mH, 500, mDM, 3])
#masses.append([4.5, sinp, mH, 500, mDM, 3])
#masses.append([5.0, sinp, mH, 500, mDM, 3])
