#!/bin/bash
#lsetup rucio
#lsetup panda

RunNumber=$1
JO=$2
dataset_name=$3
njobs=$4
version=$5

athenaTag="20.7.9.9.27,MCProd"
cmtConfig="x86_64-slc6-gcc47-opt"

output="tmp_run_01._00001.events.events,evt.pool.root*,DAOD_TRUTH3.pool.root"


# Check JO file exists
if [ -f $JO ]
then 
    
    echo ${JO} ${output} ${dataset_name} >> submission.${version}.log


    prun \
	--outDS=${dataset_name} \
	--athenaTag=$athenaTag --nJobs ${njobs} --unlimitNumOutputs --long \
	--exec="sh run_evgen_truth_monoZ_ll_2HDMa.sh ${RunNumber} ${JO} %RNDM:123" \
	--extFile ${JO} \
	--outputs=${output} \
	--maxAttempt=2 >> submission.${version}.log 2>&1


# --allowTaskDuplication \
# --disableAutoRetry
# --skipScout 
# --cmtConfig=$cmtConfig 

else
    echo "JO File Not Found"
fi