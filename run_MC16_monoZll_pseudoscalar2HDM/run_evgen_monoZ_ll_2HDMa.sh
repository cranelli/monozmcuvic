#!/bin/bash

RunNumber=$1
JOBOPTNAME=$2
RDMSEED=$3

## unpack the job options
#tar -xzvf jobOptions.monoZll_2HDMa.tar.gz

## Athena setup for showering
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#source $AtlasSetup/scripts/asetup.sh 19.2.5.14.5,MCProd,here
#source $AtlasSetup/scripts/asetup.sh 19.2.5.29.1,MCProd,here    
source  $AtlasSetup/scripts/asetup.sh 19.2.5.33.3,MCProd,here
#source $AtlasSetup/scripts/asetup.sh 19.2.5.34,AtlasProduction, here
#source $AtlasSetup/scripts/asetup.sh 19.2.5.35,AtlasProduction,here    
#source $AtlasSetup/scripts/asetup.sh 19.2.5.37,AtlasProduction, here
export MAKEFLAGS="-j1 QUICK=1 -l8"
export ATHENA_PROC_NUMBER=8

## do the showering
Generate_tf.py --ecmEnergy 13000 \
 --runNumber ${RunNumber} --firstEvent 1 \
 --maxEvents 10000 --randomSeed $RDMSEED \
 --jobConfig $JOBOPTNAME \
 --outputEVNTFile evt.pool.root

unset ATHENA_PROC_NUMBER

