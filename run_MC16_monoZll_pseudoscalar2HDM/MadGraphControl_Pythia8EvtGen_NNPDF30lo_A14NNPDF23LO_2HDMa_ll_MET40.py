from MadGraphControl.MadGraphUtils import *
import math
import os

fcard = open('proc_card_mg5.dat', 'w')



# gg initiated (4FS massive b-quarks)
if ggInitiated:
  # h1's don't contribute because sinbma = 1.0, excluding them with '/h1' 
  fcard.write("""
              import model Pseudoscalar_2HDM -modelname
              generate g g > xd xd~ l+ l- / h1  [QCD] 
              output -f
              """)

# bb initiated (5FS massless b-quarks)
else:   
  fcard.write("""
              import model Pseudoscalar_2HDM-bbMET_5FS -modelname
              generate p p > xd xd~ l+ l- / h1 
              output -f
              """)
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

extras = { 'lhe_version': '3.0',
           'cut_decays': 'F',
           'pdlabel': "'lhapdf'",
           'lhaid': 260000,
           #'parton_shower' :'PYTHIA8',
           'use_syst' : 'True',
           'systematics_program' : 'systematics',
           'systematics_arguments':"['--mur=0.5,1,2','--muf=0.5,1,2','--dyn=-1','--pdf=errorset,CT14nlo@0,MMHT2014nlo68cl@0']",
           'drll' : '0.1',
           }
"""
else:
  extras = {
          'lhe_version': '3.0',
          'cut_decays': 'F',
          'pdlabel': "'lhapdf'",
          'lhaid': 260000,
          'use_syst': 'False',
          #'maxjetflavor'  : 5,
          #'asrwgtflavor'  : 5,
          'drll' : '0.1',
          }
"""

process_dir = new_process()

if LHE_EventMultiplier>0:
  nevents=runArgs.maxEvents*LHE_EventMultiplier

#Multiplier applied to maxEvents to account for the MET filter efficiency
build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy, extras=extras)
print_cards()

paramcard = subprocess.Popen(['get_files','-data','MadGraph_param_card_Pseudoscalar2HDM.dat'])
paramcard.wait()
if not os.access('MadGraph_param_card_Pseudoscalar2HDM.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('MadGraph_param_card_Pseudoscalar2HDM.dat','r')
    newcard = open('param_card.dat','w')
    import re
    THDM_regexp = re.compile('\s+([0-9]+)\s+([0-9+-.e]+)\s+#\s+(\w+)\s*')
    for line in oldcard:
        isTHDMparam = False
        for param_name, newvalue in THDMparams.items():
          if param_name in line and "NUMBERS" not in line:
            THDM_match = THDM_regexp.match(line.rstrip('\n'))
            if THDM_match:
              THDM_pdgID = int(THDM_match.group(1))
              THDM_oldvalue = float(THDM_match.group(2))
              THDM_param_name = str(THDM_match.group(3))
              if THDM_param_name != param_name:
                print param_name, THDM_param_name
                raise RuntimeError('Mismatching parameter names, please double-check logic')
              newcard.write('     %d %s # %s\n' % (THDM_pdgID, str(newvalue), THDM_param_name))
              isTHDMparam = True
            else:
              print line.rstrip('\n')
              raise RuntimeError('Unable to parse line')
          #if "# MB" in line:           
        if not isTHDMparam:
          newcard.write(line)
    oldcard.close()
    newcard.close()

# Hack for copying MadLoop card in PROC directory. Needed for MG 2.6.2
if ggInitiated:
  from shutil import copyfile
  copyfile(os.environ['MADPATH']+'/Template/loop_material/StandAlone/Cards/MadLoopParams.dat',process_dir+'/Cards/MadLoopParams_default.dat')
  
  # Another hack for making IREGI compile
  os.environ['FC'] = "gfortran"
  """
  import fileinput, subprocess
  for line in fileinput.input(process_dir+'/Source/IREGI/src/makefile_ML5_lib', inplace=True):
    if "FC=gfortran" in line :
      print "FC=gfortran"
    else:
      print line,
  command="sed -i \"s/        /\t/g\" {0}/Source/IREGI/src/makefile_ML5_lib".format(process_dir)
  process = subprocess.Popen(command, shell=True)
  """


runName='run_01'

generate(run_card_loc='run_card.dat',
         param_card_loc='param_card.dat',
         mode=0,
         njobs=1,
         run_name=runName,
         nevents=nevents,
         proc_dir=process_dir)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz', lhe_version=3, saveProcDir=True)


#### Shower   
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    #Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

evgenConfig.description = "Generates MonoZ(ll) events  for Pseudoscalar_2HDM"
evgenConfig.keywords = ["exotic","BSM","WIMP"]


if ggInitiated:
  evgenConfig.process = "g g > xd xd~ l+ l-"
else: 
  evgenConfig.process = "p p > xd xd~ l+ l-"
evgenConfig.contact = ["Christopher Anelli <cranelli@cern.ch>"]
evgenConfig.inputfilecheck = runName

runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
                                                                                                             

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

#bonus_file = open('pdg_extras.dat','w')
#bonus_file.write('52\n')
#bonus_file.write('-52\n')
#bonus_file.close()
#testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'

#particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)
genSeq.Pythia8.Commands += ["1000022:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXd'])),
                            "1000022:isVisible = false"]
#                            "UncertaintyBands:doVariations=on",
#                            'UncertaintyBands:List = {Var3cUp isr:muRfac=0.549241,Var3cDown isr:muRfac=1.960832,isr:muRfac=0.5_fsr:muRfac=0.5 isr:muRfac=0.5 fsr:muRfac=0.5,isr:muRfac=2.0_fsr:muRfac=2.0 isr:muRfac=2.0 fsr:muRfac=2.0}']
#genSeq.Pythia8.ShowerWeightNames = ["Var3cUp", "Var3cDown","isr:muRfac=0.5_fsr:muRfac=0.5", "isr:muRfac=2.0_fsr:muRfac=2.0"]

#genSeq.Pythia8.Commands += ["52:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXd'])),
#                            "52:isVisible = false"]

#MET Filter
include("MC15JobOptions/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 40*GeV
