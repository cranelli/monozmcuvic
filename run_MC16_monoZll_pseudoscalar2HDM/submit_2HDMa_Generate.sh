#!/bin/bash
#asetup 19.2.5.14.5, MCProd, here
#lsetup rucio
#lsetup panda

RunNumber=$1
JO=$2
tanb=$3
sinp=$4
mH=$5
ma=$6
njobs=$7

export MAKEFLAGS="-j1 QUICK=1 -l8"
export ATHENA_PROC_NUMBER=8

if [ -f $JO ]
then

#echo  ${JO} ${output} >> submission.log

    echo "pathena --trf" '"'"Generate_tf.py --ecmEnergy=13000. --maxEvents=5000 --runNumber=${RunNumber} --firstEvent=1 --randomSeed=%RNDM:100 --outputEVNTFile=%OUT.evt.pool.root --jobConfig=${JO}" '"' " --outDS user.cranelli.2HDMa_MonoZ_ll_MET40_slashh1_tanb${tanb}_sinp${sinp}_mH${mH}_ma${ma}.v9 --split ${njobs}" >> submission.log.v2

    pathena --trf "Generate_tf.py --ecmEnergy=13000. --maxEvents=5000 --runNumber=${RunNumber} --firstEvent=1 --randomSeed=%RNDM:100 --outputEVNTFile=%OUT.evt.pool.root --jobConfig=${JO}" --outDS user.cranelli.2HDMa_MonoZ_ll_slashh1_tanb${tanb}_sinp${sinp}_mH${mH}_ma${ma}.v9 --split ${njobs} >> submission.log.v2 2>&1

else 
    echo "JO File not Found"
fi