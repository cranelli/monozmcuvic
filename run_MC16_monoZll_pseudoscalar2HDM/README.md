# Instructions

> python createJOFiles_mHeqmA_MET40.py v1 --jobs=1 
Automatically creates JO files, script for submitting jobs to the grid, batch.sh, 
a record of the submitted job, submit.sh, and a script for downloading the generated
LOG, LHE, and DAOD files, recovery.sh

> ./batch.sh

> cd LOCATION_TO_STORE

> ./recover.sh