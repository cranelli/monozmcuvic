#!/usr/bin/env python

import os
import sys
import subprocess
import argparse as ap

# Script to rename log files for MC request
def rename_for_jira(DIR, OUT_DIR):
    contents = os.listdir(DIR)
    samples = filter(lambda d: "user." in d , contents)

    for sample in samples:
        print sample
        base = sample.split(".")[2]
        #print base
        base = base.replace("JIRA_", "")
        # print base

        for root, dirs, filenames in os.walk(DIR + "/" + sample):
            if "log.generate" in filenames:
                log_filename = root + "/log.generate"
                # print log_filename
            
                # Check exit code and extract run number
                exit_code = -1
                flog = open(log_filename, 'r')
                for line in flog:
                    # run number
                    index1 = line.find("MC15.")
                    if(index1>0):
                        run_number = line.split(".")[1]
                        # print run_number
                        # exit code
                    index2 = line.find("INFO leaving with")
                    if(index2>0):
                        exit_code = int(line.split('code')[1].split(':')[0])
                        # print log_filename
                flog.close()
            
                # Rename and mv log file
                if exit_code == 0:
                    command = "cp " + root + "/log.generate " + OUT_DIR + "/" + run_number + "." + base + ".log.generate"
                    print command
                    os.system(command)
                else:
                    print "WARNING",  run_number, base, "job failed" 

if __name__ == '__main__':

    parser = ap.ArgumentParser( description = '', formatter_class=ap.RawTextHelpFormatter )
    parser.add_argument( 'dir', type=str,
                         help = ( 'PATH to directory of log files') )
    parser.add_argument( 'out_dir', type=str,
                         help = ("Path to output directory ofr JIRA formatted log files") )
    args = parser.parse_args()

    if len(sys.argv) < 3:
         print "\n ERROR supply path to log files \n"
    print args.dir
    rename_for_jira(args.dir, args.out_dir)
