**NOTE:**  This repository contains scripts for submitting jobs to the GRID.
Change references in the code (ie createJOFiles.py) to your username.

# Simplified DM (Vector and Axial-vector)
```
> cd run_MC16_monoZll_DMsimp_NLO
```
This directory contains scripts for generating the benchmark DM simplified models at NLO.
Handles both axial and vector models, with and without leptonic couplings, 
(A1, A2, V1, V2)  [ https://arxiv.org/pdf/1703.05703.pdf ]

## MadGraphControl File
A common control file is used for all four benchmark models:   "MadGraphControl_aMcAtNloPythia8EG_NN30NLO_A14N23LO_dmSimp_monoZll.py"  
It handles, among other things:
* The MadGraph process: "p p > xd xd~ z [QCD]" (NLO), 
* The PDF set (260000, NNPDF30_nlo_as_0118), 
* The decay of the Z Boson with MadSpin (full spinmode  to account for polarization and the non-zero decay width)
* Showering and Hadronization (Pythia8).

## Preparing JO Files
createJOFiles.py produces the JO files.  Code is flexible enough that the same 
scripts can be used to generate any of the four bencmark DM models (A1, A2, V1, V2). 
Parameter values are set in the model_XX.py files.
```
> python createJOFiles.py A1 v01 --jobs 1
```
Where the first argument specifies the model, the second argument the version,to  help track development and ensures that output datasets on the grid have unique names, and --jobs specifies the number of grid jobs per sample.  
Running "createJOFiles.py" produces secondary scripts for submiting and downloading jobs to the grid: batch_panda_XX.sh and recovery_XX.sh.  

## Grid Submission

**Note** Requires a GRID certificate 
**Note**  The batch_panda_XX.sh script is produced by createJOFiles.py
```
 asetup 19.2.5.19.3,MCProd,here
 lsetup rucio 
 lsetup panda
 ./batch_panda_A1.sh
```
The batch script submits all the jobs together.  Recorded in "submission.v##.log".  
Within the script, individual job submission is handled by   "prun_monoZll.sh".  
The output dataset name follows the format:  
user.{NAME}.NLO_dm{MODEL}_Zll_MadSpin_DM{DM}_MM{MM}.{VERSION}
Within "prun_monoZll.sh" the executed script, "run_evgen_truth_monoZll.sh" calls the 
ATHENA transforms to generate events and to convert the events into TRUTH DAODs.  
This is where the number of events per jobs are specified.

## Recovery
**Note** Requires a GRID certificate 
**Note**  The recovery_XX.sh script is produced by createJOFiles.py
```
 cd /hep300/data-shared/MonoZ/Rel21/mcuser/MC16_JIRA_DMSimp_NLO_NoPSWeights
 lsetup rucio 
 ./recovery_A1.sh
 ```
 Downloads the produced DAOD, EVT, LHE, and log files.  Separates the different file types into their own directories.

## Parton Shower Variation
Following a similar workflow as above, JOs with different PS variations can be setup 
by calling "createJOFiles_PS.py"
```
python createNloJOFiles_PS.py A1 v01 --jobs 1
```
Each JO needs a unique run number to distinguish PS variations for the same mass point, so 
incrementation of the run number must be uncommonted.  
Commands for grid submission and recovery are the same as above.

# 2HDM+a (MG Reweight) 
This directory contains scripts for generating the benchmark 2HDM+a models and implementing the
MG Reweighting Tool.  Automatically handles both bb and ggF induced processes.  [ https://arxiv.org/pdf/1810.09420.pdf]

```
> cd run_MC16_monoZll_pseudoscalar2HDM_MGReweight
```

## MadGraphControl File
A common control file is used for the 2HDM+a benchmark models:   
"MadGraphControl_Pythia8EvtGen_NNPDF30nlo_A14NNPDF23LO_2HDMa_ll_MET40_RW.py"  
It handles, among other things:
* The ggF induced process: 
⋅⋅* import model Pseudoscalar_2HDM -modelname # 4FS
⋅⋅* generate g g > xd xd~ l+ l- / h1  [QCD] 
* The bb induced process:
⋅⋅*  import model Pseudoscalar_2HDM-bbMET_5FS -modelname #5FS
⋅⋅* generate p p > xd xd~ l+ l- / h1 
* The PDF set (260000, NNPDF30_nlo_as_0118), 
* Showering and Hadronization (Pythia8).
* Parsing of the JO to produce a MG reweight card.
**NOTE:**  "/ h1" tells MadGraph to ignore diagrams that have an intermediate SM Higgs.  This is allowed, since in the allignment limit the hZa and hZA couplings are 0.

## Preparing JO Files
createJOFiles_RW.py produces the JO files.  Automatically produces both bb and ggF induced samples.
Fixed parameter values are set in benchmark_mHeqmA_MET40.py.
```
> python createJOFiles_RW.py v308 --jobs 1
```
Where the first argument specifies the version, to help track development and ensures that output datasets on the grid have unique names, and --jobs specifies the number of grid jobs per sample.  
Running "createJOFiles_RW.py" produces secondary scripts for submiting and downloading jobs to the grid: batch.sh and recovery.vXX.sh.  

## Grid Submission

**Note** Requires a GRID certificate 
**Note**  The batch.sh script is produced by createJOFiles.py
```
 asetup 20.7.9.9.27,MCProd,here
 lsetup rucio 
 lsetup panda
 ./batch.sh
```
The batch script submits all the jobs together.  Recorded in "submission.v##.log".  
Within the batch script, individual job submission is handled by   "submit_2HDMa.sh".  
The output dataset name follows the format:  
user.{NAME}.2HDMa_{IS}_MonoZ_ll_MET40_tb{TANB}_sp{SINP}_mH{MH}_ma{Ma}_mDM{MDM}_MGReweight.{VERSION}
Within "submit_2HDMa.sh." the executed script, "run_evgen_truth_monoZll.sh" calls the 
ATHENA transforms to generate events and to convert the events into TRUTH DAODs.  
This is where the number of events per jobs is specified.

## Recovery
**Note** Requires a GRID certificate 
**Note**  The recovery_XX.sh script is produced by createJOFiles.py
```
 cd /hep300/data-shared/MonoZ/Rel21/mcuser/signal_2HDMa_MGReweight
 lsetup rucio 
 ./recovery.vXX.sh
 ```

# Support Scripts

Before extracting information from the logfiles, they must first be untarred:  
```
./untar_logs.sh /hep300/data/cranelli/monoZ/MC16_2HDMa/Logs
 ```
<!-- 
extractXsec.py extracts xsecs, filtereffs, and run numbers to store in a txt file:  
> ./extractXsec.py /hep300/data/cranelli/monoZ/MC16_2HDMa/Logs
 -->
To rename the log files into the format expected by a JIRA request
```
mkdir JIRA_MC16_2HDMa_Logs
./rename_for_jira.py /hep300/data-shared/MonoZ/Rel21/mcuser/signal_2HDMa_MGReweight/Logs JIRA_MC16_2HDMa_Logs
```

Create spreadsheet for MC request ( One still need to fill in the # of events required)
```
./make_request_spreadsheet.py JIRA_MC16_2HDMa_Logs  
```

//---------- (Deprecated)  ----------//

**NOTE:**  The DMSimp is an NLO request.  Due to known bug, it does not store the PS weights.
To see an example **deprecated** of storing the PS weights one can look in:
/home/cranelli/MonoZ/GenerateTemplates/run_MC16_monoZll_MadSpin_DMsimp_NLO  

**NOTE:**  Versions of the 2HDM+a request - prior to implementing MG Reweighting - exists in: 
```
cd run_MC16_monoZll_pseudoscalar2HDM
```
However to produce samples without MG weights, it is still recommended to use the code in "run_MC16_monoZll_pseudoscalar2HDM_MGReweight" and in createJOFiles.py, leave the rw argument 
empty.
