#!/usr/bin/env python

import os
import sys
import argparse as ap

sys.path.append(os.path.abspath(os.path.curdir))

# python script to extract cross section and filter efficiency and save them in a file.

###############################################################################


def make_spreadsheet(log_dir):
    outFileName = log_dir.lstrip("JIRA_").rstrip("Logs") + "request_spreadsheet.csv"

    # Request Spreadsheet
    f = open(outFileName, 'w')
    f.write("short_name  JO  Cross Section (pb)  FilterFactor \n")
    
    # Loop over log files
    for log_filename in os.listdir(log_dir):
        print log_filename
        if "log.generate" not in log_filename: continue
        # open log file and read in values
        cross_section = 0
        filter_eff = 0
        JO = ""
        short_name = ""
        
        flog = open(log_dir + '/' + log_filename, 'r')
        for line in flog:
            # cross section
            index1 = line.find('MetaData: cross-section')
            if (index1>0):
                list1 = line.split(' ')
                #print list1
                cross_section = float(list1[4])
                #Convert from nb to pb
                cross_section *=1000

            # filter efficiency
            index2 = line.find('INFO Filter Efficiency')
            if (index2>0):
                list2 = line.split(' ')
                filter_eff = float(list2[9])
            
            # JO
            index3 = line.find("MC15.")
            if (index3>0):
                JO = line.split('"')[1]
                short_name = JO.split(".")[2]
                #print short_name

        flog.close()

        line = "%12s, %12s, %12s, %12s \n" %( short_name, JO, cross_section, filter_eff) 
        f.write(line)

    f.close()


if __name__ == '__main__':
    parser = ap.ArgumentParser( description = '', formatter_class=ap.RawTextHelpFormatter )
    parser.add_argument( 'dir', type=str,
                         help = ( 'PATH to directory of JIRA log files') )
    args = parser.parse_args()

    if len(sys.argv) < 2:
         print "\n ERROR supply path to log files \n"
    print args.dir
    make_spreadsheet(args.dir)
