#!/bin/bash
#lsetup rucio
#lsetup panda

RunNumber=$1
JO=$2
MGControl=$3
dataset_name=$4
njobs=$5
version=$6

athenaTag="20.7.9.9.27,MCProd"
#athenaTag=""19.2.5.33.3,MCProd"
cmtConfig="x86_64-slc6-gcc47-opt"

#output="tmp_run_01._00001.events.events,evt.pool.root*,DAOD_TRUTH1.pool.root"
output="tmp_run_01._00001.events.events,evt.pool.root*,DAOD_TRUTH3.pool.root"

# Check that JO File exists
if [ -f $JO ]
then
    echo ${JO} ${output} ${dataset_name} >> submission.${version}.log

    prun \
	--outDS=${dataset_name} \
	--athenaTag=$athenaTag --nJobs ${njobs} --unlimitNumOutputs --long \
	--exec="sh run_evgen_truth_monoZ_ll_2HDMa.sh ${RunNumber} ${JO} %RNDM:123" \
	--extFile ${JO},${MGControl} \
	--outputs=${output} \
	--maxAttempt=2 >> submission.${version}.log 2>&1

# --allowTaskDuplication \
# --disableAutoRetry
# --skipScout 
# --cmtConfig=$cmtConfig 
# --extFile MadGraphControl_Pythia8EvtGen_NNPDF30lo_A14NNPDF23LO_2HDMa_ll_MET40.py,${JO} \

else
    echo "JO File Not Found"
fi
