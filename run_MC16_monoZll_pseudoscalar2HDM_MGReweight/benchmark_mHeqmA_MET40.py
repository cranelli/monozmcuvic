#!/usr/bin/env python

name="equal_mHmA_MET40"
model="2HDMa"

####################################################################################
jo_template = """THDMparams = {}
THDMparams['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.
THDMparams['tanbeta'] =  %(tanb)s # The ratio of the vacuum expectation values $\tan \beta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427.
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = %(sinp)s # The sine of the mixing angle $\theta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['MXd'] = %(mDM)s # The mass of the fermionic DM candidate denoted by $m_\chi$ in arXiv:1701.07427.
THDMparams['mh1'] = 125. # The mass of the lightest scalar mass eigenstate $h$, which is identified in arXiv:1701.07427 with the Higgs-like resonance found at the LHC.
THDMparams['mh2'] = %(mH)s. # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh3'] = %(mH)s. # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details. 
THDMparams['mhc'] = %(mH)s. # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh4'] = %(ma)s. # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.

PartonShowerJO = "%(parton_shower_jo)s" #Used For PS Sytematics
LHE_EventMultiplier = %(lhe_event_multiplier)s #For Filter, Multiplier on maxEvents
reweight = %(doRW)s #Determines whether to store alternative weights for sinp and tanb
reweights = %(reweights)s

include("%(MGControl)s")
"""
####################################################################################
