#!/bin/bash

RunNumber=$1
JOBOPTNAME=$2
RDMSEED=$3

## unpack the job options
#tar -xzvf jobOptions.monoZll_2HDMa.tar.gz

## Athena setup for showering
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#source $AtlasSetup/scripts/asetup.sh 19.2.5.29.1,MCProd,here    
#source $AtlasSetup/scripts/asetup.sh 19.2.5.35,AtlasProduction,here
#source $AtlasSetup/scripts/asetup.sh 19.2.5.33.3,MCProd
source $AtlasSetup/scripts/asetup.sh 20.7.9.9.27,MCProd,here

export MAKEFLAGS="-j1 QUICK=1 -l8"
export ATHENA_PROC_NUMBER=8

## do the showering
Generate_tf.py --ecmEnergy 13000 \
 --runNumber ${RunNumber} --firstEvent 1 \
 --maxEvents 10000 --randomSeed $RDMSEED \
 --jobConfig $JOBOPTNAME \
 --outputEVNTFile evt.pool.root

## Athena setup to get truth derivation
# !! truth DAOD can only run with 1 core
unset ATHENA_PROC_NUMBER
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#source $AtlasSetup/scripts/asetup.sh 20.1.8.1,AtlasDerivation,here
source $AtlasSetup/scripts/asetup.sh 21.2.68.0, AthDerivation,here

## truth ntuple
#Reco_tf.py --inputEVNTFile evt.pool.root* --outputDAODFile pool.root --reductionConf TRUTH1; \

Reco_tf.py --inputEVNTFile evt.pool.root* --outputDAODFile pool.root --reductionConf TRUTH3;