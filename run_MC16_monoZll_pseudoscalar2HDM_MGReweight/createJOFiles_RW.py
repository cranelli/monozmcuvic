#!/usr/bin/env python
import os
import sys
import benchmark_mHeqmA_MET40 as benchmark
import argparse as ap

sys.path.append(os.path.abspath(os.path.curdir))

# python script to create jobOption files and command files

###############################################################################
cmd_template = """
./submit_2HDMa.sh  %(RunNum)s %(JO)s %(MGControl)s %(dataset)s %(jobs)s %(ver)s
"""                                                             
###############################################################################

rec_template="""
#rucio download %(dataset)s_EXT0/ --dir EVT/
rucio download %(dataset)s.log/ --dir Logs/
rucio download %(dataset)s_DAOD_TRUTH3.pool.root --dir DAOD 
rucio download %(dataset)s_tmp_run_01._00001.events.events/ --dir LHE
"""

###############################################################################

# mass points
masses = []


# Assum mH = mA = mH+
#masses.append([initial_state, tanb, sinp mH, ma, mDM, lhe_event_multiplier, rw])
lhe_event_multiplier = 1.1

# No Reweighting (Validation)
masses.append([ 1.0, 0.35, 225,  100, 10, 2])
masses.append([ 1.0, 0.35, 600,  200, 10, lhe_event_multiplier ])
masses.append([ 1.0, 0.35, 700,  400, 10, lhe_event_multiplier ])
masses.append([ 1.0, 0.35, 800,  450, 10, lhe_event_multiplier ])
masses.append([ 1.0, 0.35, 1000, 350, 10, lhe_event_multiplier ])
masses.append([ 1.0, 0.35, 1000, 600, 10, lhe_event_multiplier ])
masses.append([ 1.0, 0.35, 1600, 100, 10, 1.25 ])
masses.append([ 1.0, 0.35, 1600, 250, 10, lhe_event_multiplier ])

masses.append([ 0.3, 0.35, 600, 100, 10, lhe_event_multiplier ])
masses.append([ 5.0, 0.35, 600, 100, 10, lhe_event_multiplier ])
masses.append([ 30.0, 0.35, 600, 100, 10, lhe_event_multiplier ]) # can ignore gg sample

masses.append([ 0.3, 0.35, 600, 400, 10, lhe_event_multiplier ])
masses.append([ 5.0, 0.35, 600, 400, 10, lhe_event_multiplier ])
masses.append([ 30.0, 0.35, 600, 400, 10, lhe_event_multiplier ]) # can ignore gg sample

masses.append([ 0.3, 0.35, 400, 250, 10, 1.5 ])
masses.append([ 5.0, 0.35, 400, 250, 10, 1.5 ])
masses.append([ 30.0, 0.35, 400, 250, 10, 1.25 ]) # can ignore gg sample

masses.append([ 0.3, 0.35, 1750, 250, 10, lhe_event_multiplier ])
masses.append([ 5.0, 0.35, 1750, 250, 10, lhe_event_multiplier ])
masses.append([ 30.0, 0.35, 1750, 250, 10, lhe_event_multiplier ]) # can ignore gg sample

# Reweight Test
########### mA-ma Scan ###########
mAma_reweights = {}
mAma_reweights["gg"]="""[
'SINP_0.35',
'SINP_0.7',
]
"""

mAma_reweights["bb"]="""[ 
'SINP_0.35',
'SINP_0.7',
]
"""

masses.append([ 1.0, 0.7, 350, 200, 10, 1.25, mAma_reweights])
masses.append([ 1.0, 0.7, 1200, 100, 10, 1.25, mAma_reweights])
masses.append([ 1.0, 0.7, 1500, 500, 10, lhe_event_multiplier, mAma_reweights])                                                

########### Sinp Scans ###########
sinp_reweights = {}
sinp_reweights["gg"] ="""[
'SINP_0.1',
'SINP_0.2',
'SINP_0.3',
'SINP_0.35',
'SINP_0.4',
'SINP_0.5',
'SINP_0.6',
'SINP_0.7',
'SINP_0.707',
'SINP_0.8',
'SINP_0.9',
]"""
sinp_reweights["bb"]="""[                                                                                                                  
'SINP_0.1',
'SINP_0.2',
'SINP_0.3',
'SINP_0.35',
'SINP_0.4',
'SINP_0.5',
'SINP_0.6',
'SINP_0.7',
'SINP_0.707',
'SINP_0.8',
'SINP_0.9',
]"""


#masses.append([ 1.0, 0.35, 1000, 350, 10, lhe_event_multiplier, sinp_reweights])
masses.append([ 1.0, 0.7, 1000, 350, 10, lhe_event_multiplier, sinp_reweights])


########### Tanb Scans ###########
tanb_reweights = {}

tanb_reweights["gg"] = """[
'SINP_0.35-TANB_0.3',
'SINP_0.35-TANB_0.5',
'SINP_0.35-TANB_1.0',
'SINP_0.35-TANB_2.0',
'SINP_0.35-TANB_3.0',
'SINP_0.35-TANB_5.0',
'SINP_0.35-TANB_7.0',
'SINP_0.35-TANB_10.0',
'SINP_0.7-TANB_0.3',
'SINP_0.7-TANB_0.5', 
'SINP_0.7-TANB_1.0',
'SINP_0.7-TANB_2.0',
'SINP_0.7-TANB_3.0',
'SINP_0.7-TANB_5.0',
'SINP_0.7-TANB_7.0',
'SINP_0.7-TANB_10.0'
]"""                                                                        

tanb_reweights["bb"] = """[
'SINP_0.35-TANB_1.0',
'SINP_0.35-TANB_2.0',
'SINP_0.35-TANB_3.0',
'SINP_0.35-TANB_5.0',
'SINP_0.35-TANB_7.0',
'SINP_0.35-TANB_10.0', 
'SINP_0.35-TANB_20.0',
'SINP_0.35-TANB_30.0',
'SINP_0.7-TANB_1.0', 
'SINP_0.7-TANB_2.0', 
'SINP_0.7-TANB_3.0',  
'SINP_0.7-TANB_5.0', 
'SINP_0.7-TANB_7.0', 
'SINP_0.7-TANB_10.0',
'SINP_0.7-TANB_20.0',
'SINP_0.7-TANB_30.0'
]"""


masses.append([ 1.0, 0.7, 600, 100, 10, lhe_event_multiplier, tanb_reweights])
#masses.append([ 1.0, 0.7, 600, 150, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 600, 250, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 600, 300, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 600, 350, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 600, 400, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 600, 450, 10, 1.25, tanb_reweights])
masses.append([ 1.0, 0.7, 600, 500, 10, 1.25, tanb_reweights])

masses.append([ 1.0, 0.7, 300, 250, 10, 2, tanb_reweights])
masses.append([ 1.0, 0.7, 400, 250, 10, 1.25, tanb_reweights])
masses.append([ 1.0, 0.7, 500, 250, 10, lhe_event_multiplier, tanb_reweights])
#masses.append([ 1.0, 0.7, 600, 250, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 700, 250, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 800, 250, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 900, 250, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 1000, 250, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 1250, 250, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 1500, 250, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 1750, 250, 10, lhe_event_multiplier, tanb_reweights])
masses.append([ 1.0, 0.7, 2000, 250, 10, lhe_event_multiplier, tanb_reweights])


########### Sinp & Tanb Scans ###########
sinp_tanb_reweights = {}  
sinp_tanb_reweights["gg"] = """[
'SINP_0.1',
'SINP_0.2',
'SINP_0.3',
'SINP_0.4',
'SINP_0.5',
'SINP_0.6',
'SINP_0.707',
'SINP_0.8',
'SINP_0.9',
'SINP_0.35-TANB_0.3',
'SINP_0.35-TANB_0.5',
'SINP_0.35-TANB_1.0',
'SINP_0.35-TANB_2.0',
'SINP_0.35-TANB_3.0',
'SINP_0.35-TANB_5.0',
'SINP_0.35-TANB_7.0',
'SINP_0.35-TANB_10.0',
'SINP_0.7-TANB_0.3',
'SINP_0.7-TANB_0.5',
'SINP_0.7-TANB_1.0',
'SINP_0.7-TANB_2.0',
'SINP_0.7-TANB_3.0',
'SINP_0.7-TANB_5.0',
'SINP_0.7-TANB_7.0',
'SINP_0.7-TANB_10.0',
]"""                                                                                               

sinp_tanb_reweights["bb"] = """[
'SINP_0.1',
'SINP_0.2',
'SINP_0.3',
'SINP_0.4',
'SINP_0.5',
'SINP_0.6',
'SINP_0.707',
'SINP_0.8',
'SINP_0.9',
'SINP_0.35-TANB_1.0', 
'SINP_0.35-TANB_2.0',
'SINP_0.35-TANB_3.0',
'SINP_0.35-TANB_5.0',
'SINP_0.35-TANB_7.0',
'SINP_0.35-TANB_10.0',
'SINP_0.35-TANB_20.0',
'SINP_0.35-TANB_30.0',
'SINP_0.7-TANB_1.0',
'SINP_0.7-TANB_2.0',
'SINP_0.7-TANB_3.0',
'SINP_0.7-TANB_5.0',
'SINP_0.7-TANB_7.0',
'SINP_0.7-TANB_10.0', 
'SINP_0.7-TANB_20.0',
'SINP_0.7-TANB_30.0' 
]"""

masses.append([ 1.0, 0.7, 600, 200, 10, lhe_event_multiplier, sinp_tanb_reweights])


def create2HDMaJOFiles(jobs, version):
    
    jo_template = benchmark.jo_template
    
    cmd_filename = "batch.sh"
    fc = open(cmd_filename, 'w')
    fc.write("#asetup 20.7.9.9.27,MCProd,here\n")
    fc.write("#lsetup rucio \n")
    fc.write("#lsetup panda \n")


    rec_filename = "recovery."+ version +".sh"
    fr = open(rec_filename, 'w')

    # dictionary to replace strings
    kw = {}
    kw['MGControl'] = "MC15JobOptions/MadGraphControl_Pythia8EvtGen_NNPDF30nlo_A14NNPDF23LO_2HDMa_ll_MET40_RW.py"
    #kw['RW'] = RW
    kw['ver'] = version
    kw["jobs"] = jobs
    
    kw['parton_shower_jo'] = "" #used for PS Systematics

    #Run Nummber
    run_number = 313182

    for mass in masses:    

        # replace strings
        kw['tanb'] = mass[0]
        kw['sinp'] = mass[1]
        kw['mH'] = mass[2]
        kw['ma'] = mass[3]
        kw['mDM'] = mass[4]
        kw['lhe_event_multiplier'] = mass[5]

        kw['tanb_s'] = str(mass[0]).replace(".", "p")
        kw['sinp_s'] = str(mass[1]).replace(".", "p")
        # kw["lam3_s"] = str(mass[6]).replace(".", "d")

        for initial_state in [ "gg", "bb"]:    
            
            #Hard Code Samples to Skip - Either because they are negligible or have already been produced
            if initial_state == "gg":
                if kw['tanb'] > 10: continue
                if kw['tanb'] == 1.0 and kw['sinp'] == 0.35 and kw['mH'] == 700 and kw['ma'] == 400 and kw['mDM']==10: continue
                if kw['tanb'] == 1.0 and kw['sinp'] == 0.35 and kw['mH'] == 800 and kw['ma'] == 450 and kw['mDM']==10: continue
            if initial_state == "bb":
                if kw['tanb'] < 0.5: continue
            ######

            kw['RunNum'] = run_number
            kw["IS"] = initial_state
            
            # Reweighting
            if len(mass) < 7:
                kw['doRW'] = "False"
                kw['reweights'] = "[]"
            else :
                kw['doRW'] = "True"
                kw['reweights'] = mass[6][initial_state]
            
            jo_str = jo_template  % kw
    
            # create JobOption file
            jo_filename =  "MC15.%(RunNum)s.MGPy8EG_2HDMa_%(IS)s_MonoZ_ll_tb%(tanb_s)s_sp%(sinp_s)s_mH%(mH)s_ma%(ma)s_mDM%(mDM)s.py" % kw
            dataset_name = "user.cranelli.2HDMa_%(IS)s_MonoZ_ll_MET40_tb%(tanb_s)s_sp%(sinp_s)s_mH%(mH)s_ma%(ma)s_mDM%(mDM)s_MGReweight.%(ver)s" % kw

            kw['JO'] = jo_filename
            kw['dataset'] = dataset_name
   
            f = open(jo_filename, 'w')
            f.write(jo_str)
            f.close()
    
            command = cmd_template % kw
            fc.write(command)
        
            recovery = rec_template %kw
            fr.write(recovery)
    
            run_number += 1

    fc.close()
    fr.close()


if __name__ == '__main__':
    
    parser = ap.ArgumentParser(description = 'Create JO Files to be used for event generation of the pseudoscalar2HDM model',
                               formatter_class=ap.RawTextHelpFormatter)

    parser.add_argument( 'version' , type=str, help="version number vXX to distinguish datasets submitted to the grid")
    parser.add_argument( '--jobs' , type=int, help="number of jobs (of 10,000 events each) submitted")
    #parser.add_argument( '--MGControl', type=str, help="MG Control File to use")
    #parser.add_argument( '--RW', type=str, help="Name to idenitify MG Reweighting")
    args = parser.parse_args()

    if len(sys.argv) < 2:
        print "\n ERROR version required to distinguish datasets \n"

    if(args.jobs==None): args.jobs=1
    #if(args.MGControl==None): args.MGControl = "MC15JobOptions/MadGraphControl_Pythia8EvtGen_NNPDF30nlo_A14NNPDF23LO_2HDMa_ll_MET40.py"

    #create2HDMaJOFiles(args.MGControl, args.jobs, args.version, args.RW)
    create2HDMaJOFiles(args.jobs, args.version)
