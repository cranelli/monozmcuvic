#!/bin/bash
# Quick script to untar the Log files for MonoZ generation
# Example (User supplies directory path)
#./untar_logs.sh /hep300/data/cranelli/monoZ/MC16_2HDMa/Logs

DIR=$1

cd $DIR
for dataset in user*;
do
    echo $dataset
    cd $dataset
    
    # Untar all the files
    for file in *.tgz*;
    do
        tar -xzf $file
    done
    cd ..
done